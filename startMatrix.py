import traceback
import os
from typing import List, NoReturn

from dagster import op, graph, job, fs_io_manager
from dagster import execute_job, reconstructable
from dagster import Failure, failure_hook, HookContext, get_dagster_logger

from matrix import input_params, resource_defs, createStatusBlob
import startMatrixTests
from runMatrixTest import detachHandler
import workflowCreds

# We want to pass the blob into the hook from every failure
def raiseFailure(status, description) -> NoReturn:
    raise Failure(metadata={"status" : status}, description=description)

#
# The failure hook is how we clean up after a failure and prevent
# later attenuation levels from trying to run. There is only one
# for a job, so this is used in other files as well. 
#
@failure_hook(required_resource_keys={"sshClient", "kiwitcms"})
def startMatrixFailureHook(context: HookContext):
    op_exception = context.op_exception
    traceback.print_tb(op_exception.__traceback__)
    mval   = op_exception.metadata_entries[0].value
    status = mval.data

    #
    # Cheesy; create a flag file to tell subsequent attenuation levels
    # that we hit a failure and should not actually run. Dagster does
    # not provide a rational mechanism to handle this situation. 
    #
    if not os.path.isfile(status["errorflag"]):
        f = open(status["errorflag"], "a+")
        f.write(op_exception.description + "\n")
        f.close()
        pass

    if not status["attached"]:
        return

    #
    # If this fails, nothing we can do.
    #
    try:
        detachHandler(context, status)
        context.log.info("Detached in the failure hook")
    except Exception as exc:
        context.log.info("Failure hook failure to detach: " + str(exc))
        context.log.info(traceback.format_exc())
        pass
    pass

#
# The point of this is to get the incoming config and create a
# blob to pass along to downstream operations. 
#
@op(config_schema={"params" : dict})
def Start(context):
    blob = createStatusBlob(context.op_config["params"])
    context.log.info(str(blob))
    return blob

#
# See if the SUT experiment needs to be started
#
@op(required_resource_keys={"portal"})
def maybeStartSutExperiment(context, status) -> bool:
    #context.log.info(str(status))
    params = status["params"]
    portal = context.resources.portal
    #context.log.info(str(portal))

    result = portal.run(action="experiment.status",
                        name=params["sut_expt_name"],
                        proj=params["proj"],
                        max_wait_time=5, errors_are_fatal=False,
                        wait_for_status=False)
    #context.log.info(str(result))

    # Return False if the experiment is allready running; do not terminate
    if result["result_code"] == 0:
        return False

    #
    # Start the experiment and wait. 
    #
    result = portal.run(action="experiment.create",
                        name=params["sut_expt_name"],
                        proj=params["proj"],
                        profile=params["sut_profile_name"],
                        sshpubkey=workflowCreds.portalCreds["sshpubkey"],
                        max_wait_time=3600,
                        # Polling Interval, does not belong here.
                        interval=30,
                        errors_are_fatal=True,
                        wait_for_status=True,
                        wait_for_execute_status=True)
    context.log.info(str(result))

    #
    # Do not want to raise an Exception here since, too big a hammer
    #
    if result["result_code"] != 0:
        context.log.error(result.error)
        return None

    return True

#
# See if the Robot experiment needs to be started
#
@op(required_resource_keys={"portal"})
def maybeStartRobotExperiment(context, status) -> bool:
    #context.log.info(str(status))
    params = status["params"]
    portal = context.resources.portal
    #context.log.info(str(portal))

    if True:
        context.log.info("Robot experiment disabled for now, not needed")
        return False

    result = portal.run(action="experiment.status",
                        name=params["robot_expt_name"],
                        proj=params["proj"],
                        max_wait_time=10,
                        errors_are_fatal=False,
                        wait_for_status=False)
    #context.log.info(str(result))

    # Return False if the experiment is already running; do not terminate
    if result["result_code"] == 0:
        return False

    #
    # Start the experiment and wait. 
    #
    result = portal.run(action="experiment.create",
                        name=params["robot_expt_name"],
                        proj=params["proj"],
                        profile=params["robot_profile_name"],
                        sshpubkey=workflowCreds.portalCreds.sshpubkey,
                        max_wait_time=3600,
                        # Polling Interval, does not belong here.
                        interval=30,
                        errors_are_fatal=True,
                        wait_for_status=True,
                        wait_for_execute_status=True)
    context.log.info(str(result))

    #
    # Do not want to raise an Exception here since, too big a hammer
    #
    if result["result_code"] != 0:
        context.log.error(result.error)
        return None

    # Since we started the experiment, we will want to terminate it.
    return True

#
# The status cannot be updated in a job or graph.
# Not to mention that they run in parallel so this is our join
# where we update the status blob and pass it on.
#
@op()
def experimentsStarted(context, status, sut_result, robot_result):
    if sut_result != None and robot_result != None:
        #
        # No failures, just record and move on
        #
        status["robot_terminate"] = robot_result
        status["sut_terminate"]   = sut_result
        return status

    if sut_result == True:
        #
        # Terminate from here. 
        #
        context.log.info("Terminating SUT experiment")
        status["sut_terminate"] = True
        maybeTerminateSutExperiment(context, status)
        pass
    
    if robot_result == True:
        #
        # Terminate from here. 
        #
        context.log.info("Terminating Robot experiment")
        status["robot_terminate"] = True
        maybeTerminateRobotExperiment(context, status)
        pass

    raiseFailure(status, "Failed to start required experiments")

@op(required_resource_keys={"sshClient"})
def setupSSHkeys(context, status):
    params    = status["params"]
    sshClient = context.resources.sshClient

    # Set up ssh key on the robot node so it can ssh into sut client/server
    try:
        index = sshClient.runRemoteCommand(params["username"],
                                           status["sut_server"],
                                           "geni-get key > /tmp/key.pem; chmod 600 /tmp/key.pem; ssh-keygen -y -f /tmp/key.pem >> /users/" + params["username"] + "/.ssh/authorized_keys2",
                                           timeout=60)
    except Exception as exc:
        raiseFailure(status, "Failed to install ssh key on server (conn)")

    if sshClient.getRemoteExitStatus(index):
        raiseFailure(description="Failed to install ssh key on SUT")
    
    # Set up ssh key on the robot node so it can ssh into sut client/server
    try:
        index = sshClient.runRemoteCommand(params["username"],
                                           status["sut_client"],
                                           "geni-get key > /tmp/key.pem; chmod 600 /tmp/key.pem; ssh-keygen -y -f /tmp/key.pem >> /users/" + params["username"] + "/.ssh/authorized_keys2",
                                           timeout=60)
    except Exception as exc:
        context.log.info(traceback.format_exc())
        raiseFailure(status, "Failed to install ssh key on client (conn)")

    if sshClient.getRemoteExitStatus(index):
        raiseFailure(status, "Failed to install ssh key on client")

    # Set up ssh key on the robot node so it can ssh into sut client/server
    try:
        index = sshClient.runRemoteCommand(params["username"],
                                           status["robot_node"],
                                           "/local/repository/stackstorm-setup.pl " + params["proj"] + "," + params["sut_expt_name"],
                                           timeout=60)
    except Exception as exc:
        context.log.info(traceback.format_exc())
        raiseFailure(status, "Failed to install ssh key on robot (conn)")

    if sshClient.getRemoteExitStatus(index):
        raiseFailure(status, "Failed to install ssh key on robot (conn)")

    return status;
    
@op()
def maybeTerminateSutExperiment(context, status, results):
    if not status["sut_terminate"] or status.params["hold_experiments"]:
        return True
    
    portal.run(action="experiment.terminate",
               name=params["sut_expt_name"],
               proj=params["proj"])
    return True

@op()
def maybeTerminateRobotExperiment(context, status, results):
    if not status["robot_terminate"] or status.params["hold_experiments"]:
        return True
    
    portal.run(action="experiment.terminate",
               name=params["robot_expt_name"],
               proj=params["proj"])
    return True

@op()
def Finished(context, results, sut_result, robot_result):
    pass

@job(
    config = {
        "ops" : {
            "Start" : {
                "config" : {
                    # The defaults can be overridden in the WEB UI.
                    "params" : input_params
                }
            }
        },
        "execution": {
            "config": {
                "multiprocess": {
                    "max_concurrent": 2,
                    "tag_concurrency_limits": [
                        {
                            "key": "runone",
                            "value": "runone",
                            "limit": 1,
                        }
                    ],
                },
            }
        },
    },
    resource_defs = resource_defs,
    hooks={startMatrixFailureHook}
)
def startMatrix():
    status = Start()
    # These run in parallel
    robot_result = maybeStartRobotExperiment(status)
    sut_result = maybeStartSutExperiment(status)
    # This is the join, save the results
    status = experimentsStarted(status, sut_result, robot_result)
    # Copy the SSH keys to the various nodes.
    status = setupSSHkeys(status)
    results = startMatrixTests.runTests(status)
    # Maybe terminate the SUT/Robot experiments.
    sut_result = maybeTerminateSutExperiment(status, results)
    robot_result = maybeTerminateRobotExperiment(status, results)
    Finished(results, sut_result, robot_result)
    pass

