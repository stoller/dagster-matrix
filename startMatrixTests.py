import traceback
import os

from dagster import op, graph, job, asset, resource, fs_io_manager
from dagster import DynamicOut, DynamicOutput, Out, Output
from dagster import Failure, failure_hook, HookContext, get_dagster_logger
from dagster import execute_job, reconstructable, Definitions
from typing import List

from matrix import input_params, resource_defs, createStatusBlob
import runMatrixTest

#
# The failure hook is how we clean up after a failure and prevent
# later attenuation levels from trying to run. 
#
@failure_hook(required_resource_keys={"sshClient", "kiwitcms"})
def startFailureHook(context: HookContext):
    op_exception = context.op_exception
    traceback.print_tb(op_exception.__traceback__)
    mval   = op_exception.metadata_entries[0].value
    status = mval.data

    #
    # Cheesy; create a flag file to tell subsequent attenuation levels
    # that we hit a failure and should not actually run. Dagster does
    # not provide anything to handle this situation. 
    #
    if not os.path.isfile(status["errorflag"]):
        f = open(status["errorflag"], "a+")
        f.write(op_exception.description + "\n")
        f.close()
        pass

    if not status["attached"]:
        return

    #
    # If this fails, nothing we can do
    #
    try:
        runMatrixTest.detachHandler(context, status)
        context.log.info("Detached in the failure hook")
    except Exception as exc:
        context.log.info("Failure hook failure to detach: " + str(exc))
        context.log.info(traceback.format_exc())
        pass
    pass

#
# The point of this is to get the incoming config and create a
# blob to pass along to downstream operations. 
#
@op(config_schema={"params" : dict})
def StartTests(context):
    blob = createStatusBlob(context.op_config["params"])
    context.log.info(str(blob))
    return blob

#
# Run a series of attenuation levels by executing an external job.
# Not used at the moment.
#
@op()
def runLevelsUsingJob(context, status):
    max_attenuation  = status["params"]["max_attenuation"]
    attenuation_step = status["params"]["attenuation_step"]
    attenuation = 0

    while attenuation <= max_attenuation:
        with execute_job(
                reconstructable(level.run),
                instance=context.instance,
                run_config = {
                    "ops" : {
                        "step0" : {
                            "config" : {
                                "params" : status,
                                "level"  : attenuation
                            }
                        }
                    }
                }
        ) as result:
            context.log.info(str(result.output_for_node("step3")))
            pass
        
        attenuation += attenuation_step
        pass
    
    return status

#
# These dynamic outs are kinda like promises I guess.
# See https://docs.dagster.io/concepts/ops-jobs-graphs/dynamic-graphs
#
@op(out=DynamicOut())
def runMatrixTests(context, status):
    max_attenuation  = status["params"]["max_attenuation"]
    attenuation_step = status["params"]["attenuation_step"]
    attenuation = 0
    outputs = []
    
    while attenuation <= max_attenuation:
        outputs.append(DynamicOutput(attenuation, mapping_key=str(attenuation)))
        attenuation += attenuation_step
        pass
    return outputs

#
# We need a place to collect all the results of the DynamicOutput array
# once they are done. 
#
@op()
def collectResults(context, status, results):
    context.log.info(results)
    return status

#
# Common function for running tests from the job or the graph.
#
def runTestsCommon(status):
    # Create a dynamic op for each level
    outputs = runMatrixTests(status)
    # The .map runs each of the levels, serially.
    results = outputs.map(lambda val: runMatrixTest.runlevel(val, status))
    # Need this to collect all the results and save them
    status = collectResults(status, results.collect())
    return status

#
# This graph is used in other modules.
#
@graph()
def runTests(status):
    return runTestsCommon(status)

#
# A noop just to make the graph look better.
#
@op()
def FinishTests(context, results):
    #context.log.info(str(results))
    pass

#
# This job just runs the tests, the experiments have all been setup already
#
@job(
    config = {
        "ops" : {
            "StartTests" : {
                "config" : {
                    # The defaults can be overridden in the WEB UI.
                    "params" : input_params
                }
            }
        },
        "execution": {
            "config": {
                "multiprocess": {
                    "max_concurrent": 2,
                    "tag_concurrency_limits": [
                        {
                            "key": "runone",
                            "value": "runone",
                            "limit": 1,
                        }
                    ],
                },
            }
        },
    },
    resource_defs = resource_defs,
    hooks={startFailureHook}
)
def startMatrixTests():
    status = StartTests()
    status = runTestsCommon(status)
    FinishTests(status)
    pass
