from dagster import Definitions
from matrix import input_params, resource_defs, createStatusBlob
import startMatrix
import startMatrixTests
import runMatrixTest

defs = Definitions(
    jobs=[startMatrix.startMatrix,
          startMatrixTests.startMatrixTests,
          runMatrixTest.runMatrixTest],
    resources=resource_defs
)
