#
# TCMS/KIWI
#
import sys
import os
import kiwi

#
# KIWI setup for the Celona gear.
#
kiwi_params = {
    "classification": "Celona",
    "product": "Celona Gear",
    "product_description": "Celona Gear",
    "plantype": "Integration",
    "testplan": "Celona Matrix Testing",
    "attach_summary": "Attach completed",
    "ping_summary": "Ping Test completed",
    "iperf_summary": "iPerf Test completed",
    "detach_summary": "Detach completed",
    # These are defaults from the Stackstorm Kiwi actions
    "testcase_status": "CONFIRMED",
    "testcase_priority": "P1",
    "testcase_category": "--default--",
    "testrun_manager": "admin",
}

#
# KIWI indicies.
#
kiwi_data = {
    "classification": None,
    "product": None,
    "version": None,
    "plantype": None,
    "testplan": None,
    "build": None,
    "testrun": None,
    "testcase_attach": None,
    "testcase_ping": None,
    "testcase_iperf": None,
    "testcase_detach": None,
    "execution_attach": None,
    "execution_ping": None,
    "execution_iperf": None,
    "execution_detach": None
}

class kiwiTCMS:
    def __init__(self, creds):
        self.creds = creds
        self.kiwi  = kiwi.KiwiTCMSBaseAction(creds)
        pass

    def setup(self, status, level):
        kiwitcms = self.kiwi
        kiwidata = status["kiwidata"]
        params   = status["params"];
        
        result = kiwitcms.run(action="classification.get",
                              name=kiwi_params["classification"])
        kiwidata["classification"] = result["id"]

        result = kiwitcms.run(action="product.ensure",
                              name=kiwi_params["classification"],
                              description=kiwi_params["product_description"],
                              classification=kiwidata["classification"])
        kiwidata["product"] = result["id"]

        result = kiwitcms.run(action="version.ensure",
                              product=kiwidata["product"],
                              value=params["celona_version"])
        kiwidata["version"] = result["id"]
    
        result = kiwitcms.run(action="plantype.ensure",
                              name=kiwi_params["plantype"])
        kiwidata["plantype"] = result["id"]
    
        result = kiwitcms.run(action="testplan.ensure",
                              name=kiwi_params["testplan"],
                              text=kiwi_params["testplan"] +
                              " version " + params["celona_version"],
                              type=kiwidata["plantype"],
                              product=kiwidata["product"],
                              product_version=kiwidata["version"])
        kiwidata["testplan"] = result["id"]

        result = kiwitcms.run(action="build.ensure",
                              name=params["celona_version"],
                              version=kiwidata["version"])
        kiwidata["build"] = result["id"]
        
        result = kiwitcms.run(action="testcase.ensure",
                              summary=kiwi_params["attach_summary"],
                              text=kiwi_params["attach_summary"],
                              testplan=kiwidata["testplan"],
                              status=kiwi_params["testcase_status"],
                              category=kiwi_params["testcase_category"],
                              priority=kiwi_params["testcase_priority"],
                              product=kiwidata["product"])
        kiwidata["testcase_attach"] = result["id"]
    
        result = kiwitcms.run(action="testcase.ensure",
                              summary=kiwi_params["ping_summary"],
                              text=kiwi_params["ping_summary"],
                              testplan=kiwidata["testplan"],
                              status=kiwi_params["testcase_status"],
                              category=kiwi_params["testcase_category"],
                              priority=kiwi_params["testcase_priority"],
                              product=kiwidata["product"])
        kiwidata["testcase_ping"] = result["id"]
    
        result = kiwitcms.run(action="testcase.ensure",
                              summary=kiwi_params["iperf_summary"],
                              text=kiwi_params["iperf_summary"],
                              testplan=kiwidata["testplan"],
                              status=kiwi_params["testcase_status"],
                              category=kiwi_params["testcase_category"],
                              priority=kiwi_params["testcase_priority"],
                              product=kiwidata["product"])
        kiwidata["testcase_iperf"] = result["id"]
    
        result = kiwitcms.run(action="testcase.ensure",
                              summary=kiwi_params["detach_summary"],
                              text=kiwi_params["detach_summary"],
                              testplan=kiwidata["testplan"],
                              status=kiwi_params["testcase_status"],
                              category=kiwi_params["testcase_category"],
                              priority=kiwi_params["testcase_priority"],
                              product=kiwidata["product"])
        kiwidata["testcase_detach"] = result["id"]

        summary = "Testing Celona gear in the Matrix (version=%s), attenuation=%d)" % (params["celona_version"], level)
        result = kiwitcms.run(action="testrun.create",
                              summary=summary,
                              manager=kiwi_params["testrun_manager"],
                              build=kiwidata["build"],
                              plan=kiwidata["testplan"])
        kiwidata["testrun"] = result["id"]

        result = kiwitcms.run(action="testrun.addcase",
                              run=kiwidata["testrun"],
                              case=kiwidata["testcase_attach"],
                              status="BLOCKED")
        kiwidata["execution_attach"] = result["id"]

        result = kiwitcms.run(action="testrun.addcase",
                              run=kiwidata["testrun"],
                              case=kiwidata["testcase_ping"],
                              status="BLOCKED")
        kiwidata["execution_ping"] = result["id"]
        
        result = kiwitcms.run(action="testrun.addcase",
                              run=kiwidata["testrun"],
                              case=kiwidata["testcase_iperf"],
                              status="BLOCKED")
        kiwidata["execution_iperf"] = result["id"]

        result = kiwitcms.run(action="testrun.addcase",
                              run=kiwidata["testrun"],
                              case=kiwidata["testcase_detach"],
                              status="BLOCKED")
        kiwidata["execution_detach"] = result["id"]
        pass

    #
    # Helper function. result is PASSED or FAILED
    #
    def updateExecution(self, status, execution, result):
        if not status["params"]["skip_tcms"]:
            self.kiwi.run(action="testexecution.update",
                          execution=status["kiwidata"][execution],
                          status=result)
            pass
        pass

    #
    # Another helper to attach files to a run.
    #
    def addAttachment(self, status, filename, content):
        if not status["params"]["skip_tcms"]:
            self.kiwi.run(action="testrun.addattachment",
                          run=status["kiwidata"]["testrun"],
                          filename=filename,
                          content=content)
            pass
        pass

    pass

