import tempfile

from dagster import fs_io_manager

import resources
import workflowCreds

MAX_ATTENUATION  = 0
ATTENUATION_STEP = 10

# These are the same input params as with Stackstorm.
input_params = {
    'proj' : "PowderTeam",
    'username' : "leebee",
    'robot_profile_name' : "testbed,robot-framework",
    'robot_expt_name' : "robot-driver",
    'sut_profile_name' : "PowderTeam,cl-matrix",
    'sut_expt_name' : "cl-matrix",
    'max_attenuation' : MAX_ATTENUATION,
    'attenuation_step' : ATTENUATION_STEP,
    'celona_version': "1.0",
    'skip_tcms' : True,
    'hold_experiments' : True,
}

#
# resource definitions for jobs.
#
resource_defs = {
    "io_manager": fs_io_manager,
    "portal"    : resources.emulabPortal.configured({"creds" :
                                                     workflowCreds.portalCreds}),
    "sshClient" : resources.sshClient.configured({"creds" :
                                                  workflowCreds.managerCreds}),
    "kiwitcms"  : resources.kiwitcms.configured({"creds" :
                                                 workflowCreds.kiwiCreds})
}

#
# Collect stuff we need for the various jobs,
#
def createStatusBlob(p):
    blob = {
        "robot_terminate" : True,
        "sut_terminate"   : True,
        "attenuation"     : 0,
        "errorflag"       : tempfile.mktemp(),
        "attached"        : False,
        "kiwidata"        : {},
        "sut_client"   : "ue1" + "." + p["sut_expt_name"] + "." + p["proj"],
        "sut_server"   : "proxy" + "." + p["sut_expt_name"] + "." + p["proj"],
        "robot_node"   : "robot" + "." + p["robot_expt_name"] + "." + p["proj"],
        "params"       : p
    }
    return blob
