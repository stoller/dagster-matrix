#
# Support library.
#
import random
import time
import sys
import os
import socket
import ipaddress
import tempfile

import paramiko

class sshClient:
    def __init__(self, config, debug=False):
        self.config  = config
        self.clients = {}
        self.counter = 0

        # Need to convert the key to a paramiko key object.
        (fd,keyfile) = tempfile.mkstemp(text=True)
        f = os.fdopen(fd,"w")
        f.write(self.config.get("private_key"))
        f.close()
        self.private_key = paramiko.RSAKey.from_private_key_file(keyfile)
        os.unlink(keyfile)
        pass

    def privKey(self):
        return self.config.get("private_key")

    def runRemoteCommand(self, username, hostname, command, timeout=None):
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            client.connect(hostname=hostname, username=username,
                           # TCP timeout, reasonable default
                           timeout=15,
                           pkey=self.private_key, allow_agent=False)
        except socket.error:
            raise ValueError('Unable to connect to ' + hostname)
        except paramiko.BadAuthenticationType:
            raise ValueError('Bad authentication type.')
        except paramiko.AuthenticationException:
            raise ValueError('Authentication failed.')
        except paramiko.BadHostKeyException:
            raise ValueError('Bad host key.')
        except paramiko.SSHException as exc:
            raise ValueError('SSH exception: ' + str(exc))

        # Need a pty for proper process termination
        stdin, stdout, _ = client.exec_command(command, get_pty=True,
                                               timeout=timeout)
        stdin.close()
        channel = stdout.channel
        channel.set_combine_stderr(True)
        # Time to start and fail
        time.sleep(2)
        if channel.exit_status_ready() and channel.recv_exit_status():
            msg = "Process terminated abnormally"
            
            if channel.recv_ready():
                error = channel.recv(1024)
                msg = msg + ": " + error.decode("UTF-8")
                pass
            raise ValueError(msg);
        index = self.counter
        self.counter = self.counter + 1
        if timeout:
            timeout = int(timeout)
            pass
        self.clients[str(index)] = {
            "ssh"     : client,
            "channel" : channel,
            "started" : time.time(),
            "timeout" : timeout
        }
        return index

    def getRemoteExitStatus(self, index):
        client = self.clients[str(index)]
        chan   = client["channel"]
        ssh    = client["ssh"]

        while not chan.exit_status_ready():
            time.sleep(1)
            if self._CheckForTimeout(index):
                chan.close()
                ssh.close()
                raise ValueError('SSH command timed out')
            pass
        return chan.recv_exit_status()

    def getRemoteResults(self, index):
        client = self.clients[str(index)]
        chan   = client["channel"]
        ssh    = client["ssh"]
        output = ""
        if chan.exit_status_ready():
            while chan.recv_ready():
                output += chan.recv(1024).decode("UTF-8")
                pass
            return output

        while not chan.exit_status_ready():
            time.sleep(1)
            if self._CheckForTimeout(index):
                chan.close()
                ssh.close()
                raise ValueError('SSH command timed out')
            
            while chan.recv_ready():
                output += chan.recv(1024).decode("UTF-8")
                pass
            pass
        return output

    # Check for timeout (if there is a timeout)
    def _CheckForTimeout(self, index):
        client  = self.clients[str(index)]

        if (client["timeout"] and
            client["timeout"] > 0 and
            time.time() > client["started"] + client["timeout"]):
            return True
        return False

    pass
