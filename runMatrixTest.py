#
# Implement a single run at an attenuation level
#
import traceback
import os, signal
from typing import NoReturn

from dagster import op, graph, job, Out, Output
from dagster import Failure, failure_hook, HookContext, get_dagster_logger
from dagster._core.events import DagsterEventType
from dagster_shell import execute_shell_command

from matrix import input_params, resource_defs, createStatusBlob

# We want to pass the blob into the hook from every failure
def raiseFailure(status, description) -> NoReturn:
    raise Failure(metadata={"status" : status}, description=description)

def updateExecution(context, status, execution, result):
    kiwitcms = context.resources.kiwitcms
    kiwitcms.updateExecution(status, execution, result)
    pass

def addAttachment(context, status, filename, content):
    kiwitcms = context.resources.kiwitcms
    kiwitcms.addAttachment(status, filename, content)
    pass

#
# The failure hook is how we clean up after a failure and prevent
# later attenuation levels from trying to run. 
#
@failure_hook(required_resource_keys={"sshClient", "kiwitcms"})
def runFailureHook(context: HookContext):
    op_exception = context.op_exception
    traceback.print_tb(op_exception.__traceback__)
    mval   = op_exception.metadata_entries[0].value
    status = mval.data

    #
    # Cheesy; create a flag file to tell subsequent attenuation levels
    # that we hit a failure and should not actually run. Dagster does
    # not provide anything to handle this situation. 
    #
    if not os.path.isfile(status["errorflag"]):
        f = open(status["errorflag"], "a+")
        f.write(op_exception.description + "\n")
        f.close()
        pass

    if not status["attached"]:
        return

    #
    # If this fails, nothing we can do
    #
    try:
        detachHandler(context, status)
        context.log.info("Detached in the failure hook")
    except Exception as exc:
        context.log.info("Failure hook failure to detach: " + str(exc))
        context.log.info(traceback.format_exc())
        pass
    pass

#
# Need to be able to do this from hook above and the detach @op() below.
#
def detachHandler(context, status):
    params    = status["params"]

    command = "robot --exitonfailure --variable USERNAME:%s --variable SERVER:%s --variable CLIENT:%s --variable DEBUG:False --variable KEYFILE:%s matrix-5g-detach.robot" % (params["username"], status["sut_server"], status["sut_client"], status["keyfile"])

    context.log.info(command)

    try:
        result = execute_shell_command(command,
                                       output_logging="STREAM", log=context.log,
                                       cwd=params["robot_dir"])
    except:
        context.log.info(traceback.format_exc())
        updateExecution(context, status, "execution_detach", "FAILED")
        raiseFailure(status, "Failed to detach (conn)")

    if result[1]:
        updateExecution(context, status, "execution_detach", "FAILED")
        raiseFailure(status, "Failed to detach")

    updateExecution(context, status, "execution_detach", "PASSED")
    return True

#
# start() drags stuff out of the incoming params to pass along.
#
@op(config_schema={"params" : dict, "level" : int},
    out={"status": Out(), "level": Out()})
def StartRun(context):
    return (createStatusBlob(context.op_config["params"]),
            context.op_config["level"])

#
# XXX Create a keyfile for robot. This needs a better approach.
#
@op(tags={"dagster/priority": "0", "runone" : "runone"},
    required_resource_keys={"sshClient"})
def createKeyfile(context, status):
    fname = "/tmp/id_rsa_perexpt"
    sshClient = context.resources.sshClient
    
    if not os.path.isfile(fname):
        f = open(fname, "w")
        f.write(sshClient.privKey() + "\n")
        f.close()
        pass
    
    status["keyfile"] = fname
    return status;

#
# Check for a previous run error before starting a new one.
#
@op(tags={"dagster/priority": "0", "runone" : "runone"},
    required_resource_keys={"sshClient", "kiwitcms"})
def checkForError(context, status, level):
    #
    # This is another way of doing this, but its just as cheesy as
    # my approach. 
    # 
    if False:
        instance = context.instance
        of_type  = DagsterEventType.STEP_FAILURE
        failed_steps = instance.get_records_for_run(context.run_id,
                                                    of_type=of_type)
        context.log.info("failed steps count " + str(failed_steps))
        if len(failed_steps.records) > 0:
            raiseFailure(status, "Aborting because of previous run failure")
        pass

    # Say CHEESE!
    if os.path.isfile(status["errorflag"]):
        raiseFailure(status, "Aborting because of previous run failure")
        return status
    
    return status

#
# Initialize the TCMS stuff if the flag is set
#
@op(tags={"dagster/priority": "1", "runone" : "runone"},
    required_resource_keys={"sshClient", "kiwitcms"})
def setupTCMS(context, status, level):
    if status["params"]["skip_tcms"]:
        context.log.warning("TCMS/KIWI is disabled")
        return status

    kiwitcms = context.resources.kiwitcms
    kiwitcms.setup(status, level)
    context.log.info(str(status["kiwidata"]))
    return status

#
# Reset the attenuation to zero so we can attach
#
@op(required_resource_keys={"sshClient", "kiwitcms"},
    tags={"dagster/priority": "2", "runone" : "runone"})
def zeroAttenuation(context, status, level):
    params    = status["params"]

    if False:
        raiseFailure(status, "Invented failure")
    
    command = "robot --exitonfailure --variable USERNAME:%s --variable SERVER:%s --variable CLIENT:%s --variable DEBUG:False --variable LEVEL:0 --variable KEYFILE:%s matrix-5g-attenuate.robot" % (params["username"], status["sut_server"], status["sut_client"], status["keyfile"])

    try:
        result = execute_shell_command(command,
                                       output_logging="STREAM", log=context.log,
                                       cwd=params["robot_dir"])
    except:
        context.log.info(traceback.format_exc())
        raiseFailure(status, "Failed to zero attenuation (conn)")

    if result[1]:
        raiseFailure(status, "Failed to zero attenuation")
    
    return status

#
# Tell the UE to attach
#
@op(required_resource_keys={"sshClient", "kiwitcms"},
    tags={"dagster/priority": "3", "runone" : "runone"})
def attach(context, status):
    params    = status["params"]

    if False:
        raiseFailure(status, "Invented failure")
    
    command = "robot --exitonfailure --variable USERNAME:%s --variable SERVER:%s --variable CLIENT:%s --variable DEBUG:False --variable LEVEL:0 --variable KEYFILE:%s matrix-5g-attach.robot" % (params["username"], status["sut_server"], status["sut_client"], status["keyfile"])

    try:
        result = execute_shell_command(command,
                                       output_logging="STREAM", log=context.log,
                                       cwd=params["robot_dir"])
    except:
        context.log.info(traceback.format_exc())
        updateExecution(context, status, "execution_attach", "FAILED")
        raiseFailure(status, "Failed to attach UE (conn)")

    if result[1]:
        updateExecution(context, status, "execution_attach", "FAILED")
        raiseFailure(status, "Failed to attach UE")
    
    updateExecution(context, status, "execution_attach", "PASSED")
    status["attached"] = True
    return status

#
# Set the attenuation for the test
#
@op(required_resource_keys={"sshClient", "kiwitcms"},
    tags={"dagster/priority": "4", "runone" : "runone"})
def setAttenuation(context, status, level):
    params    = status["params"]
    
    command = "robot --exitonfailure --variable USERNAME:%s --variable SERVER:%s --variable CLIENT:%s --variable DEBUG:False --variable LEVEL:%d --variable KEYFILE:%s matrix-5g-attenuate.robot" % (params["username"], status["sut_server"], status["sut_client"], level, status["keyfile"])

    try:
        result = execute_shell_command(command,
                                       output_logging="STREAM", log=context.log,
                                       cwd=params["robot_dir"])
    except:
        context.log.info(traceback.format_exc())
        raiseFailure(status, "Failed to set attenuation (conn)")

    if result[1]:
        raiseFailure(status, "Failed to set attenuation")
        
    return status

#
# Get the Gateway IP (the IP we ping).
#
@op(required_resource_keys={"sshClient", "kiwitcms"},
    tags={"dagster/priority": "5", "runone" : "runone"})
def getIP(context, status):
    params    = status["params"]

    # Note --quiet so we get just the IP. 
    command = "robot --quiet --exitonfailure --variable USERNAME:%s --variable SERVER:%s --variable CLIENT:%s --variable DEBUG:False --variable KEYFILE:%s matrix-5g-getip.robot" % (params["username"], status["sut_server"], status["sut_client"], status["keyfile"])

    try:
        result = execute_shell_command(command,
                                       output_logging="STREAM", log=context.log,
                                       cwd=params["robot_dir"])
    except:
        context.log.info(traceback.format_exc())
        raiseFailure(status, "Failed to get gateway IP (conn)")

    if result[1]:
        raiseFailure(status, "Failed to get gateway IP")

    IP = result[0]
    context.log.info(IP)
    status["IP"] = IP
    return status

#
# Ping the gateway, And get the results
#
@op(required_resource_keys={"sshClient", "kiwitcms"},
    tags={"dagster/priority": "6", "runone" : "runone"})
def pingTest(context, status):
    params    = status["params"]
    sshClient = context.resources.sshClient

    command = "robot --exitonfailure --variable USERNAME:%s --variable SERVER:%s --variable CLIENT:%s --variable DEBUG:False --variable KEYFILE:%s matrix-5g-ping.robot" % (params["username"], status["sut_server"], status["sut_client"], status["keyfile"])

    try:
        result = execute_shell_command(command,
                                       output_logging="STREAM", log=context.log,
                                       cwd=params["robot_dir"])
    except:
        context.log.info(traceback.format_exc())
        updateExecution(context, status, "execution_attach", "FAILED")
        raiseFailure(status, "Failed to ping gateway IP (conn)")

    if result[1]:
        updateExecution(context, status, "execution_attach", "FAILED")
        raiseFailure(status, "Failed to ping gateway IP")

    #
    # Get the output file. This needs more thought. 
    #
    command = "cd /var/tmp/robot-iperf; /bin/cat results.ping"

    try:
        index = sshClient.runRemoteCommand(params["username"],
                                           status["sut_client"],
                                           command, timeout=30);
    except:
        context.log.info(traceback.format_exc())
        updateExecution(context, status, "execution_ping", "FAILED")
        raiseFailure(status, "Failed to get ping results (conn)")

    if sshClient.getRemoteExitStatus(index):
        updateExecution(context, status, "execution_ping", "FAILED")
        raiseFailure(status, "Failed to get ping results")

    results = sshClient.getRemoteResults(index)
    context.log.info(results)

    updateExecution(context, status, "execution_ping", "PASSED")
    addAttachment(context, status, "ping_results", results)
    status["ping_results"] = results
    return status

#
# Run iperf and get the results
#
@op(required_resource_keys={"sshClient", "kiwitcms"},
    tags={"dagster/priority": "7", "runone" : "runone"})
def iperfTest(context, status):
    params    = status["params"]
    sshClient = context.resources.sshClient

    command = "robot --exitonfailure --variable USERNAME:%s --variable SERVER:%s --variable CLIENT:%s --variable DEBUG:False --variable TIMEOUT:60 --variable KEYFILE:%s matrix-5g-iperf.robot" % (params["username"], status["sut_server"], status["sut_client"], status["keyfile"])

    try:
        result = execute_shell_command(command,
                                       output_logging="STREAM", log=context.log,
                                       cwd=params["robot_dir"])
    except:
        context.log.info(traceback.format_exc())
        updateExecution(context, status, "execution_iperf", "FAILED")
        raiseFailure(status, "Failed to start iperf tests (conn)")

    if result[1]:
        updateExecution(context, status, "execution_iperf", "FAILED")
        raiseFailure(status, "Failed to start iperf tests")

    output = result[0]
    #context.log.info(output)

    #
    # Get the output files. Needs more thought.
    #
    command = "cd /var/tmp/robot-iperf; /bin/cat results.upload"

    try:
        index = sshClient.runRemoteCommand(params["username"],
                                           status["sut_client"],
                                           command, timeout=30);
    except:
        context.log.info(traceback.format_exc())
        updateExecution(context, status, "execution_iperf", "FAILED")
        raiseFailure(status, "Failed to get iperf upload results (conn)")

    if sshClient.getRemoteExitStatus(index):
        updateExecution(context, status, "execution_iperf", "FAILED")
        raiseFailure(status, "Failed to get iperf upload results")

    upload_results = sshClient.getRemoteResults(index)
    addAttachment(context, status, "upload_results", upload_results)
    context.log.info(upload_results)
    
    command = "cd /var/tmp/robot-iperf; /bin/cat results.download"

    try:
        index = sshClient.runRemoteCommand(params["username"],
                                           status["sut_client"],
                                           command, timeout=30);
    except:
        context.log.info(traceback.format_exc())
        updateExecution(context, status, "execution_iperf", "FAILED")
        raiseFailure(status, "Failed to get iperf download results (conn)")

    if sshClient.getRemoteExitStatus(index):
        updateExecution(context, status, "execution_iperf", "FAILED")
        raiseFailure(status, "Failed to get iperf download results")

    download_results = sshClient.getRemoteResults(index)
    context.log.info(download_results)
    
    addAttachment(context, status, "loadload_results", download_results)
    updateExecution(context, status, "execution_iperf", "PASSED")
    status["iperf_download_results"] = download_results
    status["iperf_upload_results"] = upload_results
    return status

#
# Detach
#
@op(required_resource_keys={"sshClient", "kiwitcms"},
    tags={"dagster/priority": "8", "runone" : "runone"})
def detach(context, status):

    # See above, we need to handle this in the failure handler as well.
    detachHandler(context, status)
    status["attached"] = False
    return status

#
# A noop just to make the graph look better.
#
@op()
def FinishRun(context, status):
    #context.log.info(str(status))
    pass
    
#
# The body of the job/graph for this file.
# 
def runlevelCommon(level, status):
    status = createKeyfile(status)
    status = checkForError(status, level)
    status = setupTCMS(status, level)
    status = zeroAttenuation(status, level)
    status = attach(status)
    status = setAttenuation(status, level)
    status = getIP(status)
    status = pingTest(status)
    status = iperfTest(status)
    status = detach(status)
    return status

@graph()
def runlevel(level, status):
    return runlevelCommon(level, status);

@job(
    config = {
        "ops" : {
            "StartRun" : {
                "config" : {
                    "params" : input_params,
                    "level"  : 0
                }
            },
        },
    },
    resource_defs = resource_defs,
    hooks={runFailureHook}
)
def runMatrixTest():
    status, level = StartRun()
    status = runlevelCommon(level, status)
    FinishRun(status)
    pass

