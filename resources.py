from dagster import resource
import portal
import support
import tcms

@resource(config_schema={"creds": dict})
def emulabPortal(context):
    creds = context.resource_config["creds"]
    return portal.EmulabBaseAction(creds)

@resource(config_schema={"creds": dict})
def sshClient(context):
    creds = context.resource_config["creds"]
    return support.sshClient(creds)

@resource(config_schema={"creds": dict})
def kiwitcms(context):
    creds = context.resource_config["creds"]
    return tcms.kiwiTCMS(creds)
